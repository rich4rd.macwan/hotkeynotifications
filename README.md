# README #



# Hotkey Notifications #

### Simple scripts to display hotkey notifications for lightweight environments like i3, awesome, etc.###

### How do I get set up? ###
* Installation
	* Run ``` ./configure ```
	* And then run 	```	sudo sh install 	``` 
* Dependencies
     * Perl (for volume notifications)
     * xbacklight (for backlight notifications)
     * cron for scheduling battery notifications
* Tweaking
     * The scripts are simple to understand. If there are still issues, contact me.
### Who do I talk to? ###

* rich4rd.macwan@gmail.com
